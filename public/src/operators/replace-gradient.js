export function elitismReplace({fitnessFn, isMinimizing=true}){
  return (solution, newSolution) => {
    if(isMinimizing){
      return fitnessFn(newSolution) <= fitnessFn(solution) ? newSolution : solution
    }
    else{
      return fitnessFn(newSolution) >= fitnessFn(solution) ? newSolution : solution
    }
  }
}

export function childReplace(){
  return (solution, newSolution) => newSolution
}