import gpu from './commons/gpu.js'
import griewank from './problems/griewank.js'
import rastrigin from './problems/rastrigin.js'
import schwefel from './problems/schwefel.js'
import hillClimbing from './algorithms/hill-climbing.js'

import { randomFloat } from './commons/solution.js'
import { gauss, powerLaw } from './commons/random.js'
import { elitismReplace, childReplace } from './operators/replace-gradient.js'
import { calcStats } from './commons/stats.js'
import { 
  randomGradient,
  newtonMomemtum,
  newtonDesc,
  newton
} from './operators/update-gradient.js'

new Vue({
  el: '#app',
  data: {
    problems: [
      { name: 'Pick a problem'},
      rastrigin,
      griewank,
      schwefel
    ],
    gradients: [
      { name: 'Pick a gradient'},
      { name: 'Newton', fn: newton },
      { name: 'Newton Descend', fn: newtonDesc },
      { name: 'Newton Momemtum', fn: newtonMomemtum },
      { name: 'Random', fn: randomGradient }
    ],
    distributions: [
      { name: 'Pick a distribution'},
      { name: 'Gauss', fn: gauss },
      { name: 'Power Law', fn: powerLaw },
    ],
    n: 2,
    h: 2,
    tab: 1,
    per: 0,
    miu: 2.0,
    sigma: 2.0,
    alpha: 2.0,
    limit: 100,
    repeat: 100,
    stat: false,
    processing: false,
    selectedProblem: 1,
    selectedGradient: 4,
    selectedDistribution: 2,
    elapseTime: '',
  },
  computed: {
    problem(){
      return this.selectedProblem && this.problems[this.selectedProblem]
    },
    showDistribution(){
      return this.selectedGradient === 4
    },
    showSigma(){
      return this.selectedGradient === 4 
    },
    showAlpha(){
      return this.selectedGradient === 4 && this.selectedDistribution === 2 ||
        this.selectedGradient === 3
    },
    showMiu(){
      return this.selectedGradient === 3
    },
    showH(){
      return this.selectedGradient === 2
    }
  },
  methods:{
    minimize($event){
      $event.preventDefault()
      this.time = Date.now()
      let problem = this.problems[this.selectedProblem]
      let gradient = this.gradients[this.selectedGradient]
      let randomFn = this.distributions[this.selectedDistribution]
      let n = parseInt(this.n)
      let h = parseFloat(this.h)
      let alpha = parseFloat(this.alpha)
      let sigma = parseFloat(this.sigma)
      let miu = parseFloat(this.miu)
      let limit = parseInt(this.limit)
      let min = problem.min
      let max = problem.max
      let fitnessFn = problem.fitnessFn
      let replaceFn = childReplace({})
      let gradientFn = problem.gradientFn
      let hessianFn = problem.hessianFn
      let updateFn
      let solutionFn = randomFloat({n, min, max})
      let count = 0

      if(this.selectedGradient === 4){
        replaceFn = elitismReplace({fitnessFn})
      }
      if(this.selectedGradient === 1){
        updateFn = gradient.fn({n, hessianFn, gradientFn, min, max})
      }
      if(this.selectedGradient === 2){
        updateFn = gradient.fn({n, h, gradientFn, min, max})
      }
      if(this.selectedGradient === 3){
        updateFn = gradient.fn({n, miu, alpha, gradientFn, min, max})
      }
      if(this.selectedGradient === 4){
        updateFn = gradient.fn({n, sigma, randomFn: randomFn.fn({alpha}), min, max})
      }
      this.processing = true
      async.timesSeries(this.repeat,(i, next) => {
        let stats = hillClimbing({solutionFn, updateFn, replaceFn, fitnessFn, limit})
        count++
        this.per = 100 * count/ this.repeat
        setTimeout(() => next(null, stats), 20)
      }, (err, stats) => {
        let distribution = this.selectedGradient === 4 && '+ ' + randomFn.name + ' Distribution'
        this.calcStats(stats, gradient.name, distribution)
        this.processing = false
      });
    },
    calcStats(stats, gradientName, distribution){
      let repeat = parseInt(this.repeat)
      let limit = parseInt(this.limit)
      let data = calcStats({repeat, limit, stats})
      let x = Array.from({length: limit}, (e, i) => i + 1)
      let names = ['Worst', 'Best', 'Mean', 'Media']
      let mode = 'lines'
      data = data.map((y, i) => ({x, y, mode, name: names[i]}))
      let title = `${gradientName} Gradient ${distribution} Stats`
      Plotly.newPlot('chart', data, { title });
      this.tab = 2;
      this.stat = {
        worst: data[0].y[limit - 1],
        best: data[1].y[limit - 1],
        mean: data[2].y[limit - 1],
        media: data[3].y[limit - 1],
      };
      this.time = (Date.now() - this.time) / 1000
      this.elapseTime = this.time.toFixed(2) + 's'
      console.log(this.stat)
    }

  }
})
