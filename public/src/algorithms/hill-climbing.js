import gpu from '../commons/gpu.js'

export default function({solutionFn, updateFn, replaceFn, fitnessFn, limit=1000}){
  let solution = solutionFn()
  let stats = []

  for(let i=0; i < limit; i++){
    let newSolution = updateFn(solution)
    solution = replaceFn(solution, newSolution);
    let fitness = fitnessFn(solution)
    stats.push(fitness)
  }

  return stats
}