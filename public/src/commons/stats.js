import gpu from './gpu.js'

export function calcStats({repeat, limit, stats}){

  let kernel = gpu
    .createKernel(function(stats){
      if(this.thread.y === 0){
        let worst = 0
        for(let i=0; i < this.constants.repeat; i++){
          let val = stats[i][this.thread.x]
          if(val > worst){
            worst = val
          }
        }
        return worst
      }
      if(this.thread.y === 1){
        let best = 2147483647
        for(let i=0; i < this.constants.repeat; i++){
          let val = stats[i][this.thread.x]
          if(val < best){
            best = val
          }
        }
        return best
      }
      if(this.thread.y === 2){
        let avg = 0
        for(let i=0; i < this.constants.repeat; i++){
          avg += stats[i][this.thread.x]
        }
        return avg / this.constants.repeat
      }
      if(this.thread.y === 3){
        let min = 2147483647
        let whoMin = stats[0][this.thread.x] 
        for(let i=0; i < this.constants.repeat; i++){
          let lower = 0
          let higher = 0
          let a = stats[i][this.thread.x]
          for(let j=0; j < this.constants.repeat; j++){
            let b = stats[j][this.thread.x]
            if(a < b){
              lower++
            }
            if(a > b){
              higher++
            }
          }
          let diff = Math.abs(lower - higher)
          if(diff == min){
            whoMin = (whoMin + a) / 2
          }
          if(diff == min){
            min = diff
          }
          if(diff < min){
            whoMin = a
          }
          if(diff < min){
            min = diff
          }
        }
        return whoMin
      }
    }, {
      constants: {repeat},
      output: [limit, 4]
    })
  return kernel(stats)
}
