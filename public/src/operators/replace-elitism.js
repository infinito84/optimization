export function elitismReplace({fitnessFn, popSize}){

  function getHalf(solutions, newGeneration, costs){
    let n = solutions.length
    for(let i = 0; i < popSize / 2; i++){
      let solution = i
      let minCost = costs[i]
      for(let j = i + 1; j < n; j++){
        if(costs[j] <= minCost){
          minCost = costs[j]
          solution = j
        }
      }
      costs[solution] = costs[i]
      costs[i] = minCost      
      newGeneration.push(solutions[solution])
    }
  }

  return (solutions, newSolutions, costs) => {
    let newCosts = fitnessFn(newSolutions)
    let newGeneration = []
    getHalf(solutions, newGeneration, costs)
    getHalf(newSolutions, newGeneration, newCosts)
    return newGeneration
  }
}