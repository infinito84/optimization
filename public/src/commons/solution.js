import gpu from './gpu.js'

let bits = 0


export function getBits(){
  return bits
}

export function randomFloat({n = 1, max = 1, min = 0}){
  let diff = max - min
  let kernel = gpu
    .createKernel(function(diff, min){
      return Math.random() * diff + min
    }, {
      output: [n]
    })
  return () => kernel(diff, min)
}


export function randomBinary({n, popSize}){
  bits = n
  let kernel = gpu
    .createKernel(function(){
      if(Math.random() >= 0.5) return 1
      else return 0
    }, {
      output: [n, popSize]
    })
  return () => kernel()
}


// uniform distribution> good
// [ 12531, 8869, 9970, 10483, 9213, 13022, 8967, 9713, 10458, 7981 ]
export function randomBinaryNumber({min, max, decimals, n, popSize}){
  let diff = Math.floor(max * Math.pow(10, decimals) - min * Math.pow(10, decimals))
  let units = 0
  while(diff >= Math.pow(10, units)) units++
  return randomBinary({n: units * n * 4, popSize})
}


// REVIEW:
// uniform distribution> bad
// [ 12742, 6908, 12872, 6855, 12972, 11993, 5958, 11722, 12003, 5975 ]
export function randomBinaryNumberBad({min, max, decimals, n}){
  let diff = Math.floor(max * Math.pow(10, decimals) - min * Math.pow(10, decimals))
  let units = 0
  while(diff >= Math.pow(10, units)) units++
  
  let kernel = gpu
    .createKernel(function(randomNumbers){
      let numberIndex = Math.floor(float(this.thread.x) * this.constants.divisor)
      let number = randomNumbers[numberIndex]
      let bitIndex = this.thread.x % 4
      return float((int(number) >> int(bitIndex)) & int(1))
    }, {
      constants: {diff, units, n, divisor: 1 / (units * 4)},
      output: [units * n * 4]
    })
  return () => {
    let randomNumbers = Array.from({length: n}, () => parseInt(Math.random() * (diff + 1)))
    return kernel(randomNumbers)
  }
}


export function keepSearchSpace(){
  let fn = function keepSearchSpace(val, random1){
    if(val < this.constants.min || val > this.constants.max){
      return random1 * (this.constants.max - this.constants.min) + this.constants.min
    }
    return val
  }
  fn.source = fn.toString()
  fn.argumentTypes = ['Number', 'Number']
  fn.argumentNames = ['val', 'random1']
  return fn
}
