import gpu from './gpu.js'

let calcKernel, toUnits, toFloats

export function binariesToFloat({min, max, decimals=0, n, popSize}){
  let diff = Math.floor(max * Math.pow(10, decimals) - min * Math.pow(10, decimals))
  let units = 0
  while(diff >= Math.pow(10, units)) units++

  let binToUnitsKernel = gpu
    .createKernel(function(solutions){
      let i = this.thread.x * 4
      let sum = 0
      for(let j = 0; j < 4; j++){
        sum += Math.pow(2.0, float(j)) * solutions[this.thread.y][i + j]
      }
      if(sum > 9){
        sum = ((sum % 10) * 10 / 6)
      }
      return sum
    }, {
      constants: {n, units},
      output: [units * n, popSize]
    })

  let unitsToFloatKernel = gpu
    .createKernel(function(solutions){
      let i = this.thread.x * this.constants.units
      let sum = 0
      for(let j = 0; j < this.constants.units; j++){
        sum += Math.pow(10.0, float(j - this.constants.decimals)) * solutions[this.thread.y][i + j]
      }
      sum = sum % this.constants.diff
      sum += this.constants.min
      return sum
    }, {
      constants: {n, units, min, decimals, diff: max - min},
      output: [n, popSize]
    })

  toUnits = binToUnitsKernel
  toFloats = unitsToFloatKernel

  const superKernel = gpu
  .combineKernels(binToUnitsKernel, unitsToFloatKernel, function(solutions) {
    return unitsToFloatKernel(binToUnitsKernel(solutions))
  });
  return (solutions) => superKernel(solutions)
}


export function fitnessFloatKernel({problem, popSize, n, decimals}){
  let fnString = problem.fitnessFn.toString()
  fnString = fnString.replace(/solution\.length/g, 'float(this.constants.n)')
  fnString = fnString.replace(/solution\[i\]/g, 'solution[this.thread.x][i]')
  fnString = fnString.replace(/solution/g, 'solutions')
  fnString = fnString.replace(/let /g, 'var ')
  fnString = fnString.split('{')
  fnString.shift()
  fnString = fnString.join('').split('}')
  fnString.pop()
  fnString = fnString.join('')

  let fitnessKernel = gpu.createKernel(Function('solutions', fnString), {
    constants: {n},
    output: [popSize]
  })

  calcKernel = binariesToFloat({
    min: problem.min, 
    max: problem.max, 
    decimals, 
    n, 
    popSize
  })
  if(!problem.isBinary){
    return (solutions) => fitnessKernel(calcKernel(solutions))
  }
  return (solutions) => fitnessKernel(solutions)
}


export function solutionsToFloats(solutions){
  return calcKernel(solutions)
}

export function solutionsToFloatsKernel(){
  return [toUnits, toFloats]
}