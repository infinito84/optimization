import gpu from '../commons/gpu.js'
import { calcProbabilities } from './selection-roulette.js'
import { getBits } from '../commons/solution.js'

export function mutationBit({popSize, bits, mutationRate}){

  let mutateKernel = gpu
    .createKernel(function(solutions){
      let bit = solutions[this.thread.y][this.thread.x]
      if(Math.random() < this.constants.prob){
        if(bit == 1) return 0
        else return 1
      }
      return bit
    }, {
      constants: {popSize, bits, prob: mutationRate * bits / getBits()},
      output: [getBits(), popSize]
    })
  console.log(mutationRate * bits / getBits())

  return (solutions) => mutateKernel(solutions)
}