import gpu from '../commons/gpu.js'

let problem = {
  min: -5.12,
  max: 5.12,
  name: 'Rastrigin',
  image: 'https://www.sfu.ca/~ssurjano/rastr.png',
  credits: 'https://www.sfu.ca/~ssurjano/rastr.html',
  formula: 'https://www.sfu.ca/~ssurjano/rastr2.png',

  fitnessFn: function fitnessFn(solution) {
    let sum = 10.0 * solution.length
    for(let i = 0; i < solution.length; i++){
      sum += solution[i] * solution[i] - 10.0 * Math.cos(2.0 * Math.PI * solution[i])
    }
    return sum;
  },

  gradientFn: function gradientFn(val, solution){
    return 2 * val + 20 * Math.PI * Math.sin(2 * Math.PI * val)
  },

  hessianFn: function hessianFn(val, solution){
    return 2 + 40 * Math.PI * Math.PI * Math.cos(2 * Math.PI * val)
  }
}

problem.gradientFn.source = problem.gradientFn.toString()
problem.gradientFn.argumentTypes = ['Number', 'Array']
problem.gradientFn.argumentNames = ['val', 'solution']

problem.hessianFn.source = problem.hessianFn.toString()
problem.hessianFn.argumentTypes = ['Number', 'Array']
problem.hessianFn.argumentNames = ['val', 'solution']

export default problem