import gpu from '../commons/gpu.js'

let problem = {
  isBinary: true,
  name: 'Max one',
  image: 'http://tracer.lcc.uma.es/problems/onemax/img/onemax.gif',
  credits: 'http://tracer.lcc.uma.es/problems/onemax/onemax.html',
  formula: 'http://tracer.lcc.uma.es/problems/onemax/img/image008.gif',

  fitnessFn: function fitnessFn(solution) {
    let sum = 0.0
    for(let i = 0; i < solution.length; i++){
      sum += solution[i]
    }
    return solution.length - sum;
  }
}

export default problem