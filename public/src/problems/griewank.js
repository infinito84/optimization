import gpu from '../commons/gpu.js'

let float = (i) => i

let problem = {
  min: -600.0,
  max: 600.0,
  name: 'Griewank',
  image: 'https://www.sfu.ca/~ssurjano/griewank.png',
  credits: 'https://www.sfu.ca/~ssurjano/griewank.html',
  formula: 'https://www.sfu.ca/~ssurjano/griewank2.png',

  fitnessFn: function fitnessFn(solution) {
    let prod = 1.0
    let sum = 1.0
    for(let i = 0; i < solution.length; i++){
      prod *= Math.cos(solution[i] / Math.sqrt(float(i)+1.0))
    }
    for(let i = 0; i < solution.length; i++){
      sum += solution[i] * solution[i] * 1.0/4000.0
    }
    sum -= prod
    return sum
  },

  gradientFn: function gradientFn(val, solution){
    let i = this.thread.x
    let constantPart = 1.0;
    for(let j=0; j < this.constants.n; j++){
      if(j !== i) constantPart = constantPart * Math.cos(solution[j] / Math.sqrt(float(j+1)))
    }
    return val/2000 + constantPart * Math.sin(val / Math.sqrt(float(i+1))) / Math.sqrt(float(i+1))
  },

  hessianFn: function hessianFn(val, solution){
    let i = this.thread.x
    let constantPart = 1.0;
    for(let j=0; j < this.constants.n; j++){
      if(j !== i) constantPart = constantPart * Math.cos(solution[j] / Math.sqrt(float(j+1)))
    }
    return  1/2000 + constantPart * Math.cos(val / Math.sqrt(float(i+1))) / float(i+1)
  }
}

problem.gradientFn.source = problem.gradientFn.toString()
problem.gradientFn.argumentTypes = ['Number', 'Array']
problem.gradientFn.argumentNames = ['val', 'solution']

problem.hessianFn.source = problem.hessianFn.toString()
problem.hessianFn.argumentTypes = ['Number', 'Array']
problem.hessianFn.argumentNames = ['val', 'solution']

export default problem