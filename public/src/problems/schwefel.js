import gpu from '../commons/gpu.js'

let problem = {
  min: -500.0,
  max: 500.0,
  name: 'Schwefel',
  image: 'https://www.sfu.ca/~ssurjano/schwef.png',
  credits: 'https://www.sfu.ca/~ssurjano/schwef.html',
  formula: 'https://www.sfu.ca/~ssurjano/schwef2.png',

  fitnessFn: function fitnessFn(solution) {
    let sum = 418.9829 * solution.length
    for(let i = 0; i < solution.length; i++){
      sum -= solution[i] * Math.sin(Math.sqrt(Math.abs(solution[i])))
    }
    return sum;
  },

  gradientFn: function gradientFn(val, solution){
    let ax = Math.abs(val)
    let sx = Math.sqrt(ax)
    return -1 * (Math.sin(sx) + Math.cos(sx) * val * val / (2 * Math.pow(ax, 3/2)))
  },

  hessianFn: function hessianFn(val, solution){
    let ax = Math.abs(val)
    let sx = Math.sqrt(ax)
    return -1 * (3 * sx * Math.cos(sx) - ax * Math.sin(sx)) / (4 * val)
  }
}

problem.gradientFn.source = problem.gradientFn.toString()
problem.gradientFn.argumentTypes = ['Number', 'Array']
problem.gradientFn.argumentNames = ['val', 'solution']

problem.hessianFn.source = problem.hessianFn.toString()
problem.hessianFn.argumentTypes = ['Number', 'Array']
problem.hessianFn.argumentNames = ['val', 'solution']

export default problem
