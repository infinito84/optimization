import gpu from './commons/gpu.js'

import griewank from './problems/griewank.js'
import rastrigin from './problems/rastrigin.js'
import schwefel from './problems/schwefel.js'
import maxOne from './problems/max-one.js'

import classic from './algorithms/genetic-classic.js'
import haea from './algorithms/evolutionary-haea.js'

import { fitnessFloatKernel } from './commons/fitness-kernel.js'
import { roulette } from './operators/selection-roulette.js'
import { tournament } from './operators/selection-tournament.js'
import { crossoverCuts } from './operators/crossover-cuts.js'
import { crossoverDiff } from './operators/crossover-differential.js'
import { mutationBit } from './operators/mutate-bits.js'

import { 
  randomBinary, 
  randomBinaryNumber
} from './commons/solution.js'
import { gauss, powerLaw } from './commons/random.js'
import { childReplace } from './operators/replace-gradient.js'
import { elitismReplace } from './operators/replace-elitism.js'
import { calcStats } from './commons/stats.js'

new Vue({
  el: '#app',
  data: {
    problems: [
      { name: 'Pick a problem'},
      maxOne,
      rastrigin,
      griewank,
      schwefel
    ],
    algorithms: [
      { name: 'Pick an algorithm'},
      { name: 'Classic', fn: classic },
      { name: 'Haea', fn: haea },
    ],
    selections: [
      { name: 'Pick a selectionFn'},
      { name: 'Roulette', fn: roulette },
      { name: 'Tournament', fn: tournament }
    ],
    crossovers: [
      { name: 'Pick a crossoverFn'},
      { name: 'Cuts', fn: crossoverCuts },
      { name: 'Differential', fn: crossoverDiff },
    ],
    operators: [
      { name: 'Pick an operator'},
      { name: 'Crossover Cuts', fn: crossoverCuts },
      { name: 'Crossover Differential', fn: crossoverDiff, isReal: true },
      { name: 'Mutation bit', fn: mutationBit}
    ],
    replaces: [
      { name: 'Pick a replaceFn'},
      { name: 'Offspring', fn: childReplace },
      { name: 'Elitism', fn: elitismReplace },
    ],
    n: 2,
    k: 10,
    tab: 1,
    per: 0,
    miu: 2.0,
    cuts: 1,
    rate: 0.5,
    F: 2.0,
    limit: 100,
    decimals: 2,
    popSize: 100,
    stat: false,
    processing: false,
    selectedProblem: 1,
    selectedAlgorithm: 1,
    selectedSelection: 1,
    selectedCrossover: 1,
    selectedReplace: 1,
    selectedOperator: 0,
    selectedOperators: [],
    mutateBits: 1,
    mutationRate: 0.1,
    elapseTime: '',
    best: -1,
    generation: -1,
    bestRates: false
  },
  computed: {
    problem(){
      return this.selectedProblem && this.problems[this.selectedProblem]
    },
    showElite(){
      return this.selectedAlgorithm === 4 
    },
    showK(){
      return this.selectedSelection === 2
    },
    showCuts(){
      return this.selectedCrossover === 1
    },
    showFRate(){
      return this.selectedCrossover === 2
    },
    showClassic(){
      return this.selectedAlgorithm === 1
    },
    showHaea(){
      return this.selectedAlgorithm === 2
    },
    filterOperators(){
      if(this.selectedProblem !== 1) return this.operators
      return this.operators.filter(o => !o.isReal)
    },
    showCuts2(){
      return this.selectedOperator === 1
    },
    showFRate2(){
      return this.filterOperators.length === 4 &&
        this.selectedOperator === 2
    },
    showMutate(){
      return this.selectedOperator === 3 || (
        this.filterOperators.length === 3 && this.selectedOperator === 2
      )
    },
  },
  methods:{
    minimize($event){
      $event.preventDefault()
      this.time = Date.now()
      let problem = this.problems[this.selectedProblem]
      let algorithm = this.algorithms[this.selectedAlgorithm]
      let selection = this.selections[this.selectedSelection]
      let crossover = this.crossovers[this.selectedCrossover]
      let replace = this.replaces[this.selectedReplace]
      let n = parseInt(this.n)
      let popSize = parseInt(this.popSize)
      let k = parseInt(this.k)
      let mutateBits = parseInt(this.mutateBits)
      let limit = parseInt(this.limit)
      let decimals = parseInt(this.decimals)
      let cuts = parseInt(this.cuts)
      let rate = parseFloat(this.rate)
      let F = parseFloat(this.F)
      let mutationRate = parseFloat(this.mutationRate)
      let min = problem.min
      let max = problem.max
      let fitnessFn = fitnessFloatKernel({problem, popSize, n, decimals})
      let solutionsFn
      if(!problem.isBinary){
        solutionsFn = randomBinaryNumber({min, max, decimals, n, popSize})
      }
      else{
        solutionsFn = randomBinary({n, popSize})
      }
      let selectionFn
      switch(this.selectedSelection){
        case 1: selectionFn = selection.fn({popSize}); break;
        case 2: selectionFn = selection.fn({popSize, k}); break;
      }
      let crossoverFn
      switch(this.selectedCrossover){
        case 1: crossoverFn = crossover.fn({cuts, popSize}); break;
        case 2: crossoverFn = crossover.fn({F, rate, popSize, n, min, max, decimals}); break;
      }
      let replaceFn
      switch(this.selectedReplace){
        case 1: replaceFn = replace.fn(); break;
        case 2: replaceFn = replace.fn({popSize, fitnessFn}); break;
      }

      this.processing = true
      let stats = algorithm.fn({
        solutionsFn, 
        fitnessFn, 
        selectionFn, 
        crossoverFn,
        mutateBits,
        mutationRate,
        replaceFn,
        popSize,
        limit,
        notify: this.notifyPer,
        end: (stats) => {
          this.calcStats(stats, algorithm.name, '')
          this.processing = false
        }
      })
    },
    notifyPer({per, best, generation, bestRates}){
      this.per = per
      this.best = best
      this.generation = generation
      this.bestRates = bestRates
    },
    calcStats(stats, gradientName, distribution){
      console.log(stats)
      let popSize = parseInt(this.popSize)
      let limit = parseInt(this.limit)
      let data = calcStats({repeat: popSize, limit: limit, stats})
      let x = Array.from({length: limit}, (e, i) => i + 1)
      let names = ['Worst', 'Best', 'Mean', 'Media']
      let mode = 'lines'
      data = data.map((y, i) => ({x, y, mode, name: names[i]}))
      let title = `${gradientName} Genetic Algorithm ${distribution}\n Stats`
      Plotly.newPlot('chart', data, { title });
      this.tab = 2;
      this.stat = {
        worst: data[0].y[limit - 1],
        best: data[1].y[limit - 1],
        mean: data[2].y[limit - 1],
        media: data[3].y[limit - 1],
      };
      this.time = (Date.now() - this.time) / 1000
      this.elapseTime = this.time.toFixed(2) + 's'
    },
    addOperator(){
      let problem = this.problems[this.selectedProblem]
      let operator = this.filterOperators[this.selectedOperator]
      let n = parseInt(this.n)
      let popSize = parseInt(this.popSize)
      let bits = parseInt(this.mutateBits)
      let decimals = parseInt(this.decimals)
      let cuts = parseInt(this.cuts)
      let rate = parseFloat(this.rate)
      let F = parseFloat(this.F)
      let mutationRate = parseFloat(this.mutationRate)
      let min = problem.min
      let max = problem.max
      let params
      let fn2 = operator.fn
      let name = operator.name

      switch(this.selectedOperator){
        case 1: 
          params = {cuts, popSize}
          this.selectedOperators.push({params, fn2, name})
          break
        case 2: 
          if(operator.isReal){
            params = {F, rate, popSize, n, min, max, decimals}
            this.selectedOperators.push({params, fn2, name})
            break
          }
        case 3:
          params = {popSize, bits, mutationRate}
          this.selectedOperators.push({params, fn2, name})
      }
    },
    haea($event){
      $event.preventDefault()
      if(this.selectedOperator.length < 2) return alert('Add operators')
      this.time = Date.now()
      let problem = this.problems[this.selectedProblem]
      let algorithm = this.algorithms[this.selectedAlgorithm]
      let selection = this.selections[this.selectedSelection]
      let n = parseInt(this.n)
      let popSize = parseInt(this.popSize)
      let k = parseInt(this.k)
      let mutateBits = parseInt(this.mutateBits)
      let limit = parseInt(this.limit)
      let decimals = parseInt(this.decimals)
      let cuts = parseInt(this.cuts)
      let rate = parseFloat(this.rate)
      let F = parseFloat(this.F)
      let min = problem.min
      let max = problem.max
      let fitnessFn = fitnessFloatKernel({problem, popSize, n, decimals})
      let solutionsFn
      if(!problem.isBinary){
        solutionsFn = randomBinaryNumber({min, max, decimals, n, popSize})
      }
      else{
        solutionsFn = randomBinary({n, popSize})
      }
      let selectionFn
      switch(this.selectedSelection){
        case 1: selectionFn = selection.fn({popSize}); break;
        case 2: selectionFn = selection.fn({popSize, k}); break;
      }
      this.selectedOperators.forEach(oper => {
        oper.params.popSize = popSize
        oper.fn = oper.fn2(oper.params)
      })

      this.processing = true
      let stats = algorithm.fn({
        solutionsFn, 
        fitnessFn, 
        selectionFn, 
        operators: this.selectedOperators,
        popSize,
        limit,
        notify: this.notifyPer,
        end: (stats) => {
          this.calcStats(stats, algorithm.name, '')
          this.processing = false
        }
      })
    }
  }
})

// let rb = randomBinaryNumber({min: 0, max: 1, decimals: 2, n: 100000})
// let c = getFloatsFromBinaries({min: 0, max: 1, decimals: 2, n: 100000, popSize: 1})

// let results = c([rb()])[0]
// let part = 10
// let group = Array.from({length: part}, (e, i) => [i * (1/ part), (i + 1) * (1/ part)])
// let parts = Array.from({length: part}, () => 0)
// for(let i=0; i < results.length; i++){
//   let a = results[i]
//   group.forEach((part, j) => {
//     if(a >= part[0] && a <= part[1]){
//       parts[j] = (parts[j] || 0) + 1
//     }
//   })
// }

// console.log(parts)