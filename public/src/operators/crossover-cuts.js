import gpu from '../commons/gpu.js'
import { getBits } from '../commons/solution.js'

export function crossoverCuts({cuts, popSize}){
  let cutsKernel = gpu
    .createKernel(function(parents, cuts){
      let i = 0
      while(cuts[i] < this.thread.x && i < this.constants.cuts){
        i++
      }
      if(this.thread.y % 2 == 0){
        if(this.thread.x <= cuts[i]){
          if(i % 2 == 0){
            // return 1
            return parents[this.thread.y][this.thread.x]
          }
          else{
            // return 2
            return parents[this.thread.y + 1][this.thread.x] 
          }
        }
        else{
          if(i % 2 == 0){
            // return 3
            return parents[this.thread.y][this.thread.x]
          }
          else{
            // return 4
            return parents[this.thread.y + 1][this.thread.x] 
          }
        }
      }
      else{
        if(this.thread.x <= cuts[i]){
          if(i % 2 == 1){
            // return 1
            return parents[this.thread.y - 1][this.thread.x] 
          }
          else{
            // return 2
            return parents[this.thread.y][this.thread.x]
          }
        }
        else{
          if(i % 2 == 1){
            // return 3
            return parents[this.thread.y - 1][this.thread.x] 
          }
          else{
            // return 4
            return parents[this.thread.y][this.thread.x]
          }
        }
      }
    }, {
      constants: {popSize, cuts},
      output: [getBits(), popSize]
    })

  return (parents) => {
    let children = []
    let bits = getBits();
    let size = bits / cuts
    let cutParts = Array.from({length: cuts}, (e, i) => parseInt(size * (i + Math.random())))


    let n = popSize / 2
    return cutsKernel(parents, cutParts)
  }
} 