export function gauss(){
  let fn = function randomFn(random1, random2){
    return Math.sqrt(-2.0 * Math.log(random1)) * Math.cos(2.0 * Math.PI * random2)
  }
  fn.source = fn.toString()
  fn.argumentTypes = ['Number', 'Number']
  fn.argumentNames = ['random1', 'random2']
  return fn
}

export function powerLaw({alpha=1.1}){
  let fn = function randomFn(random1, random2){
    if(random1 > 0.5){
      return Math.pow(1 - random2, 1 / (1 - alpha));
    }
    return -Math.pow(1 - random2, 1 / (1 - alpha));
  }
  let functionString = fn.toString().replace(/alpha/g, alpha)
  fn = Function('random1', 'random2', functionString)
  fn.source = functionString
  fn.argumentTypes = ['Number', 'Number']
  fn.argumentNames = ['random1', 'random2']
  return fn
}