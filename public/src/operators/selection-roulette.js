import gpu from '../commons/gpu.js'


export function calcProbabilities(costs){
  let sum = 0
  let max = costs[0]
  let min = costs[0]
  for(let i=0; i<costs.length; i++){
    sum += costs[i]
    max = costs[i] > max ? costs[i] : max
    min = costs[i] < min ? costs[i] : min
  }
  let acum = 0
  let probs = costs.map((cost, i) => {
    let prob = (max + min - cost) / sum
    acum += prob
    return prob
  })
  let previous = 0
  return probs.map((prob, i) => {
    previous = previous + prob / acum
    return previous
  })
}

export function roulette({popSize}){

  let rouletteKernel = gpu
    .createKernel(function(probs){
      let r1 = Math.random();
      let i = 0
      while(probs[i] <= r1) i++
      return i
    }, {
      constants: {popSize},
      output: [popSize]
    })

  return (solutions, costs) => {
    let probs = calcProbabilities(costs)
    let result = rouletteKernel(probs)
    return Array.from({length: popSize}, (e, i) => solutions[result[i]])
  }
}