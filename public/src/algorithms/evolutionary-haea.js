import gpu from '../commons/gpu.js'
import { mutationBit } from '../operators/mutate-bits.js'

let allRates = {}
let allOperators = []

function normalizeRates(rates){
  let sum = rates.reduce((total, r) => total + r, 0)
  return rates.map(r => r / sum)
}

function extractRates(i){
  if(allRates[i]) return allRates[i]
  let n = allOperators.length
  allRates[i] = Array.from({length: n}, () => Math.random())
  return normalizeRates(allRates[i])
}

function opSelect(rates){
  let i = 0
  let r = Math.random()
  while(r < rates[i] && i < rates.length) i++
  return i === rates.length ? i - 1 : i
}

function getBest(children, fitnessFn, ind, cost){
  let costs = fitnessFn(children)
  let result = {cost, child: ind}
  for(let i = 0; i < children.length; i++){
    if(costs[i] < result.cost){
      result.cost = costs[i] 
      result.child = children[i]
    }
  }
  return result
}

export default function({
  solutionsFn, 
  selectionFn, 
  operators,
  fitnessFn, 
  limit=1000,
  popSize,
  notify,
  end
}){
  let solutions = solutionsFn()
  let stats = Array.from({length: popSize}, () => [])
  let generation = -1
  allRates = {}
  allOperators = operators
  let best = Infinity
  let bestRates = []

  async.timesSeries(limit,(t, next) => {
    let costs = fitnessFn(solutions)
    let newGeneration = []
    for(let j = 0; j < popSize; j++){
      let ind = solutions[j]
      let rates = extractRates(j)

      let alpha = Math.random()
      let oper = opSelect(rates)
      let parents = selectionFn(solutions, costs)
      let children = operators[oper].fn(parents, costs, fitnessFn)
      let result = getBest(children, fitnessFn, ind, costs[j])
      if(result.cost < costs[j]){
        rates[oper] = (1 + alpha) * rates[oper]
      }
      else{
        rates[oper] = (1 - alpha) * rates[oper] 
      }
      allRates[j] = normalizeRates(rates)
      newGeneration.push(result.child)

      stats[j].push(result.cost)
      if(result.cost < best){
        best = result.cost
        generation = t + 1
        bestRates = allRates[j].slice(0)
      }
    }
    solutions = newGeneration
    if(notify) notify({
      per: 100 * (t +1) / limit,
      best,
      generation,
      bestRates
    })
    setTimeout(() => next(), 10)
  }, () => end && end(stats))
}