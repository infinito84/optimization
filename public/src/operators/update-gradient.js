import gpu from '../commons/gpu.js'
import { keepSearchSpace } from '../commons/solution.js'


export function randomGradient({n, sigma, randomFn, min, max}){
  let kernel = gpu
    .createKernel(function(solution){
      let val = solution[this.thread.x]
      let r1 = Math.random()
      let r2 = Math.random()
      let r3 = Math.random()
      return keepSearchSpace(val + this.constants.sigma * randomFn(r1, r2), r3)
    }, {
      constants: {n, sigma, min, max},
      output: [n],
      functions: [randomFn, keepSearchSpace()]
    })
  return (solution) => kernel(solution)
}


export function newtonMomemtum({n, miu, alpha, gradientFn, min, max}){
  let deltas = Array.from({length: n}, () => 0)

  let calcDelta = function calcDelta(gradient, delta){
    return this.constants.miu * delta - gradient * this.constants.alpha;
  }
  calcDelta.source = calcDelta.toString()
  calcDelta.argumentTypes = ['Number', 'Number']
  calcDelta.argumentNames = ['gradient', 'delta']

  let calcDeltaKernel = gpu
    .createKernel(function(solution, deltas){
      let val = solution[this.thread.x]
      let gradient = gradientFn(val, solution)
      return calcDelta(gradient, deltas[this.thread.x])
    }, {
      constants: {n, miu, alpha},
      output: [n],
      functions: [calcDelta, gradientFn]
    })

  let kernel = gpu
    .createKernel(function(solution, deltas){
      let r1 = Math.random()
      return keepSearchSpace(solution[this.thread.x] + deltas[this.thread.x], r1)
    }, {
      output: [n],
      constants: {min, max},
      functions: [keepSearchSpace()]
    })

  return (solution) => {
    deltas = calcDeltaKernel(solution, deltas)
    return kernel(solution, deltas)
  }
}


export function newtonDesc({n, h, gradientFn, min, max}){
  let kernel = gpu
    .createKernel(function(solution){
      let val = solution[this.thread.x]
      let gradient = gradientFn(val, solution)
      let r1 = Math.random()
      return keepSearchSpace(val + this.constants.h * gradient, r1)
    }, {
      output: [n],
      constants: {n, h, min, max},
      functions: [gradientFn, keepSearchSpace()]
    })
  return (solution) => kernel(solution)
}


export function newton({n, hessianFn, gradientFn, min, max}){
    let kernel = gpu
      .createKernel(function(solution){
        let val = solution[this.thread.x]
        let gradient = gradientFn(val, solution)
        let hessian = hessianFn(val, solution)
        let r1 = Math.random()
        return keepSearchSpace(val - gradient / hessian, r1)
      }, {
        output: [n],
        constants: {n, min, max},
        functions: [gradientFn, hessianFn, keepSearchSpace()]
      })
  return (solution) => kernel(solution)
}
