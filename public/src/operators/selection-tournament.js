import gpu from '../commons/gpu.js'
import { calcProbabilities } from './selection-roulette.js'

export function tournament({popSize, k}){
  let contestantsKernel = gpu
    .createKernel(function(){
      return Math.floor(Math.random() * this.constants.popSize)
    }, {
      constants: {popSize, k},
      output: [k, popSize]
    })

  let tournamentKernel = gpu
    .createKernel(function(contestants, probs){
      let r1 = Math.random();
      let i = 0
      while(probs[this.thread.x][i] <= r1) i++
      return contestants[this.thread.x][i]
    }, {
      constants: {popSize, k},
      output: [popSize]
    })

  return (solutions, costs) => {
    let contestants = contestantsKernel()
    let probs = contestants.map(c => calcProbabilities(c.map(i => costs[i])))
    let result = tournamentKernel(contestants, probs)
    return Array.from({length: popSize}, (e, i) => solutions[result[i]])
  }
}