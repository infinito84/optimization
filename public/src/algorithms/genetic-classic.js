import gpu from '../commons/gpu.js'
import { mutationBit } from '../operators/mutate-bits.js'

export default function({
  solutionsFn, 
  selectionFn, 
  crossoverFn,
  mutateBits,
  mutationRate,
  replaceFn, 
  fitnessFn, 
  limit=1000,
  popSize,
  notify,
  end
}){
  let mutationFn
  if(mutateBits > 0){
    mutationFn = mutationBit({popSize, bits: mutateBits, mutationRate})
  }

  let solutions = solutionsFn()
  let stats = Array.from({length: popSize}, () => [])
  let best = Infinity
  let generation = -1

  async.timesSeries(limit,(gen, next) => {
    let costs = fitnessFn(solutions)
    let parents = selectionFn(solutions, costs)
    let children = crossoverFn(parents, costs, fitnessFn)
    if(mutationFn){
      children = mutationFn(children)
    }
    solutions = replaceFn(solutions, children, costs)

    costs.forEach((c, i) => {
      stats[i].push(c)
      if(c < best){
        best = c
        generation = gen
      }
    })
    if(notify) notify({
      per: 100 * (gen +1) / limit,
      best,
      generation
    })
    setTimeout(() => next(null, stats), 20)
  }, () => end && end(stats))
}