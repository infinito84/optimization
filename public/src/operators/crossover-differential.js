import gpu from '../commons/gpu.js'
import { getBits } from '../commons/solution.js'
import { solutionsToFloatsKernel } from '../commons/fitness-kernel.js'

export function crossoverDiff({popSize, n, F, rate, min, max, decimals}){
  let differentParents = gpu
    .createKernel(function(example){
      let i = example[this.thread.x]
      let j = Math.floor(Math.random() * this.constants.popSize)
      while(i == j) j = Math.floor(Math.random() * this.constants.popSize)
      return j
    }, {
      constants: {popSize},
      output: [popSize]
    })

  let nextInd = gpu
    .createKernel(function(R, values, a, b, c){
      if(Math.random() < this.constants.rate || this.thread.x == Math.floor(R)){
        let aIndex = a[this.thread.y]
        let bIndex = b[this.thread.y]
        let cIndex = c[this.thread.y]
        return values[aIndex][this.thread.x] + this.constants.F * (
          values[bIndex][this.thread.x] - 
          values[cIndex][this.thread.x]
        )
      }
      else{
        return values[this.thread.y][this.thread.x]
      }
    }, {
      constants: {popSize, F, rate},
      output: [n, popSize]
    })

  let example = Array.from({length: popSize}, () => -1)
  let diff = Math.floor(max * Math.pow(10, decimals) - min * Math.pow(10, decimals))
  let units = 0
  while(diff >= Math.pow(10, units)) units++
  
  let backToBinaries = gpu
    .createKernel(function(values){
      let numberIndex = Math.floor(float(this.thread.x) * this.constants.divisor)
      let number = values[this.thread.y][numberIndex]
      number = number * Math.pow(10.0, float(this.constants.decimals))
      number -= this.constants.min * Math.pow(10.0, float(this.constants.decimals))
      let bitIndex = this.thread.x % 4
      return float((((int(number) << (int(30) -int(bitIndex))) >> (int(30) - int(bitIndex))) >> int(bitIndex)) & int(1))
      // return float((int(number) >> int(bitIndex)) & int(1))
    }, {
      constants: {diff, units, n, divisor: 1 / (units * 4), decimals, min},
      output: [units * n * 4, popSize]
    })

  let [toUnits, toFloats] = solutionsToFloatsKernel()
  console.log(toUnits, toFloats)
  const superKernel = gpu.combineKernels(
    differentParents, toUnits, toFloats, nextInd, backToBinaries,
  function(parents, example, n) {
    let a = differentParents(example)
    let b = differentParents(a)
    let c = differentParents(b)
    let values = toFloats(toUnits(parents))
    let R = Math.floor(Math.random() * n)
    values = nextInd(R, values, a, b, c)
    return backToBinaries(values)
  });


  return (parents, costs, fitnessFn) => {
    let children = superKernel(parents, example, n)
    let costs2 = fitnessFn(children)
    return parents.map((parent, i) => costs2[i] <= costs[i] ? children[i] : parent)
  }
} 
